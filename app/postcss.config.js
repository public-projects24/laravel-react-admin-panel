export default {
    plugins: {
        "tailwindcss/nesting": {}, // Add support for nesting
        tailwindcss: {},
        autoprefixer: {},
        "postcss-pxtorem": {
            //rootValue: 16,
            propList: ["*"],
        },
    },
};
