<?php

namespace App\Http\Requests\Frontend\Users;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {

        //dd($this->user);
        return [
            'username' => 'required|string|max:255|unique:users,username,' . $this->user,
            'role_id' => 'required|integer',
            'active' => 'required|boolean',
            'password' => 'sometimes|required|confirmed|string|max:255',
        ];
    }
}
