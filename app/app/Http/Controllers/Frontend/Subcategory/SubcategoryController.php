<?php

namespace App\Http\Controllers\Frontend\Subcategory;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Subcategories\CreateSubcategoryRequest;
use App\Http\Requests\Frontend\Subcategories\UpdateSubcategoryRequest;
use App\Models\Subcategory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SubcategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkAdminUserAuth')->only('store','update','destroy');
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $categoryId = $request->category;
        
        if($categoryId) {
            return response([
                'data' =>  Subcategory::OrderBy('id','desc')
                ->where('category_id', $categoryId)->get(),
            ], Response::HTTP_OK);
        }

        return response([
            'data' =>  Subcategory::OrderBy('id','desc')->paginate(10),
        ], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateSubcategoryRequest $request)
    {
        try {
            $subcategory = Subcategory::create($request->safe()->only([
                'name',
                'active',
                'category_id'
            ]));
    
            return response([
                'message' => 'subcategory created',
                'subcategory' =>  $subcategory,
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Server Error',
                'description' => config('app.debug') ? $e->getMessage()
                    : 'Please activate debug mode to see the error message.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }    
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            return response([
                'data' =>  Subcategory::findOrFail($id),
            ], Response::HTTP_OK); 
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => '404 not found'
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSubcategoryRequest $request, string $id)
    {
        try {
            $subcategory = Subcategory::findOrFail($id);
            $subcategory->update($request->safe()->only([
                'name',
                'active',
                'category_id'
            ]));
                
            return response([
                'message' => 'subcategory updated',
                'subcategory' =>  $subcategory,
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Server Error',
                'description' => config('app.debug') ? $e->getMessage()
                    : 'Please activate debug mode to see the error message.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } 
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $subcategory = Subcategory::findOrFail($id);
            $subcategory->delete();
                  
            return response([
                'message' => 'subcategory deleted',
                'subcategory' =>  $subcategory,
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Server Error',
                'description' => config('app.debug') ? $e->getMessage()
                    : 'Please activate debug mode to see the error message.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } 
    }
}
