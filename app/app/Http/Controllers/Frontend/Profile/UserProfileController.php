<?php

namespace App\Http\Controllers\Frontend\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Profile\UpdateUserProfileRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        return response([
            'data' => $request->user()
        ], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserProfileRequest $request, int $id )
    {
        try {   
            $user =$request->user();

            if($user->id !== $id) {
                return response([
                    'message' => 'the user cannot perfom this action. The user is trying to update another user profile',
                ], Response::HTTP_FORBIDDEN);
            }
            
            $user->update($request->safe()->only([
                'username',
            ]));

            if ($request->password) {
                $user->update([
                    'password' => Hash::make($request->password),
                ]);
            }
                
            return response([
                'message' => 'user updated',
                'user' =>  $user,
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Server Error',
                'description' => config('app.debug') ? $e->getMessage()
                    : 'Please activate debug mode to see the error message.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }    
    }
}
