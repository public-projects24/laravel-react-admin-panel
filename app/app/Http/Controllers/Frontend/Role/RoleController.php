<?php

namespace App\Http\Controllers\Frontend\Role;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RoleController extends Controller
{
    public function index()
    {
        return response([
            'data' =>  Role::all(),
        ], Response::HTTP_OK);
    }
}
