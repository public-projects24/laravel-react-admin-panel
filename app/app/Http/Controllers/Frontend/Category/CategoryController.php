<?php

namespace App\Http\Controllers\Frontend\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Categories\CreateCategoryRequest;
use App\Http\Requests\Frontend\Categories\UpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkAdminUserAuth')->only('store','update','destroy');
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if($request->getAll) {
            return response([
                'data' =>  Category::OrderBy('id','desc')->get(),
            ], Response::HTTP_OK);
        }

        return response([
            'data' =>  Category::OrderBy('id','desc')->paginate(10),
        ], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateCategoryRequest $request)
    {
        try {
            $category = Category::create($request->safe()->only([
                'name',
                'active',
            ]));
    
            return response([
                'message' => 'category created',
                'category' =>  $category,
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Server Error',
                'description' => config('app.debug') ? $e->getMessage()
                    : 'Please activate debug mode to see the error message.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }    
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            return response([
                'data' =>  Category::findOrFail($id),
            ], Response::HTTP_OK);   
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => '404 not found'
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCategoryRequest $request, string $id)
    {
        try {
            $category = Category::findOrFail($id);
            $category->update($request->safe()->only([
                'name',
                'active',
            ]));
                
            return response([
                'message' => 'category updated',
                'category' =>  $category,
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Server Error',
                'description' => config('app.debug') ? $e->getMessage()
                    : 'Please activate debug mode to see the error message.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } 
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $category = Category::findOrFail($id);
            $category->delete();
                  
            return response([
                'message' => 'category deleted',
                'category' =>  $category,
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Server Error',
                'description' => config('app.debug') ? $e->getMessage()
                    : 'Please activate debug mode to see the error message.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } 
    }
}
