<?php

namespace App\Http\Controllers\Frontend\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Products\CreateProductRequest;
use App\Http\Requests\Frontend\Products\UpdateProductRequest;
use App\Http\Resources\Frontend\Product\ProductResource;
use App\Models\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkAdminUserAuth')->only('store','update','destroy');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        DB::enableQueryLog();
        return ProductResource::collection(
            Product::with(['subcategory','subcategory.category'])
            ->OrderBy('id','desc')
            ->paginate(10)
        );
        dd(DB::getQueryLog());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateProductRequest $request)
    {
        try {
            $product = Product::create($request->safe()->only([
                'name',
                'subcategory_id'
            ]));
    
            return response([
                'message' => 'product created',
                'product' =>  $product,
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Server Error',
                'description' => config('app.debug') ? $e->getMessage()
                    : 'Please activate debug mode to see the error message.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }   
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            return response([
                'data' =>  Product::findOrFail($id),
            ], Response::HTTP_OK); 
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => '404 not found'
            ], Response::HTTP_NOT_FOUND);
        } 
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProductRequest $request, string $id)
    {
        try {
            $product = Product::findOrFail($id);
            $product->update($request->safe()->only([
                'name',
                'subcategory_id'
            ]));
                
            return response([
                'message' => 'product updated',
                'product' =>  $product,
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Server Error',
                'description' => config('app.debug') ? $e->getMessage()
                    : 'Please activate debug mode to see the error message.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } 
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $product = Product::findOrFail($id);
            $product->delete();
                  
            return response([
                'message' => 'product deleted',
                'product' =>  $product,
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Server Error',
                'description' => config('app.debug') ? $e->getMessage()
                    : 'Please activate debug mode to see the error message.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } 
    }
}
