<?php

namespace App\Http\Controllers\Frontend\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Frontend\Users\CreateUserRequest;
use App\Http\Requests\Frontend\Users\UpdateUserRequest;
use App\Http\Resources\Frontend\User\UserResource;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PhpParser\Node\Stmt\TryCatch;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkAdminUserAuth')->only('store','update','destroy');
    }

    public function index() 
    {
        // 1. Get pagination
        $paginatedData = User::OrderBy('id','desc')->paginate(10);
        
        // 2. Exclude data info
        $paginatedCopy = $paginatedData;
        $paginator     = $paginatedCopy->toArray();
        unset($paginator['data']);


        return response([
            'data' => UserResource::collection($paginatedData),
            'pagination' => $paginator
        ], Response::HTTP_OK);
    }

    public function store(CreateUserRequest $request)
    {
        try {
            $user = User::create([
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'role_id' => $request->role_id,
                'active' => $request->active,
            ]);
    
            return response([
                'message' => 'user created',
                'user' =>  $user,
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Server Error',
                'description' => config('app.debug') ? $e->getMessage()
                    : 'Please activate debug mode to see the error message.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }    
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            return response([
                'data' =>  User::findOrFail($id),
            ], Response::HTTP_OK); 
        } catch (ModelNotFoundException $e) {
            return response([
                'message' => '404 not found'
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserRequest $request, string $id)
    { 
        try {
            $user = User::findOrFail($id);
            $user->update($request->safe()->only([
                'username',
                'role_id',
                'active',
            ]));

            if ($request->password) {
                $user->update([
                    'password' => Hash::make($request->password),
                ]);
            }
                
            return response([
                'message' => 'user updated',
                'user' =>  $user,
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Server Error',
                'description' => config('app.debug') ? $e->getMessage()
                    : 'Please activate debug mode to see the error message.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }    
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();
                  
            return response([
                'message' => 'user deleted',
                'user' =>  $user,
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Server Error',
                'description' => config('app.debug') ? $e->getMessage()
                    : 'Please activate debug mode to see the error message.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } 
    }
}
