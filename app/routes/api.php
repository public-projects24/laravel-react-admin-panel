<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RefreshTokenController;
use App\Http\Controllers\Frontend\Category\CategoryController;
use App\Http\Controllers\Frontend\Product\ProductController;
use App\Http\Controllers\Frontend\Profile\UserProfileController;
use App\Http\Controllers\Frontend\Role\RoleController;
use App\Http\Controllers\Frontend\Subcategory\SubcategoryController;
use App\Http\Controllers\Frontend\Users\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', [LoginController::class, 'index']);
Route::post('refresh-token', [RefreshTokenController::class, 'index'])
                ->middleware('checkRefreshToken:sanctum');
Route::post('logout', [LogoutController::class, 'index']);

Route::middleware('auth:sanctum')->group(function() {
    Route::resource('roles', RoleController::class);
    Route::resource('profile', UserProfileController::class);
    Route::resource('users', UserController::class);
    Route::resource('categories', CategoryController::class);
    Route::resource('subcategories', SubcategoryController::class);
    Route::resource('products', ProductController::class);
});
