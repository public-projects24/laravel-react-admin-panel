import React from "react";
import { Outlet } from "react-router-dom";
import TitleSection from "@/components/common/TitleSection";

export default function CategoriesPage() {
    return (
        <div>
            <div className="section container mx-auto">
                <TitleSection title="Categories" />
                <Outlet />
            </div>
        </div>
    );
}
