import React from "react";
import { Outlet } from "react-router-dom";
import TitleSection from "@/components/common/TitleSection";

export default function UsersPage() {
    return (
        <div>
            <div className="section container mx-auto">
                <TitleSection title="Users" />
                <Outlet />
            </div>
        </div>
    );
}
