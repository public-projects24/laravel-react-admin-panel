import React from "react";
import { Outlet } from "react-router-dom";
import TitleSection from "@/components/common/TitleSection";

export default function ProductsPage() {
    return (
        <div>
            <div className="section container mx-auto">
                <TitleSection title="Products" />
                <Outlet />
            </div>
        </div>
    );
}
