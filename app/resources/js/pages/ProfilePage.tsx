import "@/assets/css/components/profile/profile.css";
import React, { useEffect, useState } from "react";
import useAxiosPrivate from "@/hooks/useAxiosPrivate";
import TitleSection from "@/components/common/TitleSection";
import { toast } from "sonner";

type User = {
    id: number;
    username: string;
    password: string | null;
    password_confirmation: string | null;
    _method: string | null;
};

export default function ProfilePage() {
    const axiosPrivate = useAxiosPrivate();
    const [user, setUser] = useState<User>();
    const [loading, setLoading] = useState(false);
    const [statusMessage, setStatusMessage] = useState(null);
    const [errorMessage, setErrorMessage] = useState(null);

    const handleSubmitForm = async (e: React.SyntheticEvent) => {
        e.preventDefault();

        setStatusMessage("Updating ...");

        try {
            await axiosPrivate.post(`/profile/${user.id}`, user);
            toast.success("Profile updated.");
        } catch (error) {
            toast.error(error.response.data.message);
        } finally {
            setStatusMessage(null);
        }
    };

    useEffect(() => {
        let isMounted = true;
        const controller = new AbortController();

        const getUser = async () => {
            try {
                setLoading(true);
                const response = await axiosPrivate.get("/profile", {
                    signal: controller.signal,
                });

                let userResponse = response.data.data;
                userResponse._method = "put";
                isMounted && setUser(userResponse);

                setLoading(false);
            } catch (error) {
                if (error.response) {
                    setErrorMessage(error.response.data.message);
                }
            }
        };
        getUser();

        return () => {
            isMounted = false;
            controller.abort();
        };
    }, []);

    return (
        <div>
            <div className="section profile container mx-auto">
                {loading && <div>Loading profile ...</div>}
                {!loading && user && (
                    <>
                        <TitleSection title="Profile" />
                        <form
                            className="form"
                            onSubmit={handleSubmitForm}
                            noValidate
                        >
                            <div className="form__form-group">
                                <label
                                    className="form__label"
                                    htmlFor="username"
                                >
                                    Username
                                </label>
                                <input
                                    className="form__input"
                                    id="username"
                                    type="text"
                                    value={user.username}
                                    onChange={(e) => {
                                        setUser({
                                            ...user,
                                            username: e?.target.value,
                                        });
                                    }}
                                ></input>
                            </div>
                            <div className="form__form-group">
                                <label
                                    className="form__label"
                                    htmlFor="password"
                                >
                                    Password
                                </label>
                                <input
                                    className="form__input"
                                    id="password"
                                    type="password"
                                    onChange={(e) => {
                                        setUser({
                                            ...user,
                                            password: e?.target.value,
                                        });
                                    }}
                                ></input>
                            </div>
                            <div className="form__form-group">
                                <label
                                    className="form__label"
                                    htmlFor="password_confirmation"
                                >
                                    Confirm Password
                                </label>
                                <input
                                    className="form__input"
                                    id="password_confirmation"
                                    type="password"
                                    onChange={(e) => {
                                        setUser({
                                            ...user,
                                            password_confirmation:
                                                e?.target.value,
                                        });
                                    }}
                                ></input>
                            </div>
                            {errorMessage && (
                                <div className="form-group form__error-message">
                                    {errorMessage}
                                </div>
                            )}
                            <button
                                className="btn btn--primary profile__button"
                                disabled={statusMessage !== null ? true : false}
                            >
                                {statusMessage !== null
                                    ? statusMessage
                                    : "Update"}
                            </button>
                        </form>
                    </>
                )}
            </div>
        </div>
    );
}
