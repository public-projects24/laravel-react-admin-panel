import api from "@/services/api";

export default {
    login(credentials: object) {
        return api.post(`login`, credentials);
    },
    logout() {
        return api.post(`logout`);
    },
};
