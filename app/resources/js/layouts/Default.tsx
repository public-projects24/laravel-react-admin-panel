import { Outlet } from "react-router-dom";
import NavMenu from "@/components/common/NavMenu";
import { Suspense } from "react";
import { Toaster } from "sonner";

function Default() {
    return (
        <>
            <NavMenu />
            <Toaster richColors />
            <div id="main-content">
                <Suspense fallback={<h1>Loading ...</h1>}>
                    <Outlet />
                </Suspense>
            </div>
        </>
    );
}

export default Default;
