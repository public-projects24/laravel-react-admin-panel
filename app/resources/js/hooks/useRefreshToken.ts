import axios from "@/services/api";

const useRefreshToken = () => {
    const refresh = async () => {
        await axios.post("refresh-token");
    };
    return refresh;
};

export default useRefreshToken;
