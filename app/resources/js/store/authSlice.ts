import auth from "@/services/auth";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import Cookies from "js-cookie";
type User = {
    id: number;
    username: string;
    role_id: number;
    active: boolean;
};

export interface AuthState {
    isAuthenticated: boolean;
    user: User;
    statusMessage: string | null;
    errorMessage: string | null;
}

const initialState: AuthState = {
    isAuthenticated:
        Cookies.get("auth") !== undefined &&
        Cookies.get("auth") !== null &&
        Cookies.get("auth") !== ""
            ? true
            : false,
    statusMessage: null,
    errorMessage: null,
};

export const loginRequest = createAsyncThunk(
    "auth/loginRequest",
    async (
        credentials: { email: string; password: string },
        { rejectWithValue }
    ) => {
        try {
            const response = await auth.login(credentials);
            return response;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const logoutRequest = createAsyncThunk(
    "auth/logoutRequest",
    async (data, { rejectWithValue }) => {
        try {
            const response = await auth.logout();
            return response;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        setUser: (state, payload: PayloadAction<User>) => {
            state.user = payload.payload;
        },
        clearErrorMessage: (state) => {
            state.errorMessage = null;
        },
    },
    extraReducers: (builder) => {
        builder.addCase(loginRequest.fulfilled, (state, action) => {
            state.statusMessage = null;
            state.errorMessage = null;

            state.isAuthenticated = true;

            Cookies.set("auth", "true");
        });
        builder.addCase(loginRequest.pending, (state) => {
            state.statusMessage = "Sign in ...";
        });
        builder.addCase(loginRequest.rejected, (state, action) => {
            state.statusMessage = null;
            state.errorMessage = (action.payload as any).response.data.message;
        });
        builder.addCase(logoutRequest.fulfilled, (state, action) => {
            state.statusMessage = null;
            state.errorMessage = null;

            state.isAuthenticated = false;
            Cookies.remove("auth");
        });
        builder.addCase(logoutRequest.pending, (state) => {
            state.statusMessage = "Log out ...";
        });
        builder.addCase(logoutRequest.rejected, (state, action) => {});
    },
});

// Action creators are generated for each case reducer function
export const { setUser, clearErrorMessage } = authSlice.actions;

export default authSlice.reducer;
