import React, { useEffect, useState } from "react";
import useAxiosPrivate from "@/hooks/useAxiosPrivate";
import { Link } from "react-router-dom";
import Pagination from "../common/Pagination";
import ErrorSection from "@/components/common/ErrorSection";
import { useAppSelector } from "@/hooks/reduxHooks";

type Record = {
    id: number;
    name: string;
    active: string;
};

export default function SubcategoryList() {
    const auth = useAppSelector((state) => state.authState);
    const axiosPrivate = useAxiosPrivate();
    const [loading, setLoading] = useState(false);
    const [records, setRecords] = useState<Record[]>([]);
    const [errorMessage, setErrorMessage] = useState(null);

    const refreshRecords = async () => {
        const response = await axiosPrivate.get("/subcategories");
        setRecords(response.data.data);
    };

    const handleDeleteRecord = async (id: number) => {
        if (window.confirm("Are you sure to delete this record?")) {
            await axiosPrivate.delete(`/subcategories/${id}`);
            refreshRecords();
        }
    };

    const handleChangePage = async (page: string) => {
        if (page) {
            const pageParameters = page.split("?");
            const nextPage = pageParameters[1];
            const response = await axiosPrivate.get(
                `/subcategories?${nextPage}`
            );
            setRecords(response.data.data);
        }
    };

    useEffect(() => {
        let isMounted = true;
        const controller = new AbortController();

        const getRecords = async () => {
            try {
                setLoading(true);
                const response = await axiosPrivate.get("/subcategories", {
                    signal: controller.signal,
                });

                isMounted && setRecords(response.data.data);
            } catch (error) {
                if (error.response) {
                    setErrorMessage(error.response.data.message);
                }
            } finally {
                setLoading(false);
            }
        };
        getRecords();

        return () => {
            isMounted = false;
            controller.abort();
        };
    }, []);

    return (
        <>
            {loading && <div>Loading subcategories ...</div>}
            {!loading && records.data && (
                <>
                    {auth.user && auth.user.role_id === 1 && (
                        <Link
                            to="/subcategories/create"
                            className="btn btn--success"
                        >
                            New subcategory
                        </Link>
                    )}

                    <table className="table-auto table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Active</th>
                                {auth.user && auth.user.role_id === 1 && (
                                    <th>Actions</th>
                                )}
                            </tr>
                        </thead>
                        <tbody>
                            {records.data.map((record) => (
                                <tr key={record.id}>
                                    <td>{record.id}</td>
                                    <td>{record.name}</td>
                                    <td>
                                        {record.active ? "active" : "inactive"}
                                    </td>
                                    {auth.user && auth.user.role_id === 1 && (
                                        <td>
                                            <Link
                                                to={`/subcategories/${record.id}/edit`}
                                                className="btn btn--info"
                                            >
                                                Edit
                                            </Link>

                                            <button
                                                onClick={() =>
                                                    handleDeleteRecord(
                                                        record.id
                                                    )
                                                }
                                                className="btn btn--danger"
                                            >
                                                Delete
                                            </button>
                                        </td>
                                    )}
                                </tr>
                            ))}
                        </tbody>
                    </table>

                    {!loading && records.links && (
                        <Pagination
                            links={records.links}
                            handleChangePage={handleChangePage}
                        />
                    )}

                    {errorMessage && <ErrorSection title={errorMessage} />}
                </>
            )}
        </>
    );
}
