import { useNavigate, useParams } from "react-router-dom";
import useAxiosPrivate from "@/hooks/useAxiosPrivate";
import React, { useState, useEffect } from "react";
import SubtitleSection from "@/components/common/SubtitleSection";
import ErrorSection from "@/components/common/ErrorSection";
import { toast } from "sonner";
type Role = {
    id: number;
    name: string;
};

type Record = {
    id: number;
    username: string;
    role_id: number | undefined;
    active: number | undefined;
    password: string | null;
    password_confirmation: string | null;
};

export default function UserForm() {
    let params = useParams();
    const axiosPrivate = useAxiosPrivate();
    const navigate = useNavigate();
    const [record, setRecord] = useState<Record>();
    const [roles, setRoles] = useState<Role[]>();
    const [loading, setLoading] = useState(false);
    const [statusMessage, setStatusMessage] = useState(null);
    const [errorMessage, setErrorMessage] = useState(null);

    const handleSubmitForm = async (e: React.SyntheticEvent) => {
        e.preventDefault();

        setStatusMessage(params.id ? "Updating ..." : "Creating ...");

        try {
            let response = null;
            if (params.id) {
                response = await axiosPrivate.post(
                    `/users/${record.id}`,
                    record
                );
            } else {
                response = await axiosPrivate.post(`/users`, record);
            }

            toast.success(response.data.message);
        } catch (error) {
            toast.error(error.response.data.message);
        } finally {
            setStatusMessage(null);
        }
    };

    useEffect(() => {
        let isMounted = true;
        const controller = new AbortController();

        const getRoles = async () => {
            try {
                const response = await axiosPrivate.get(`/roles`, {
                    signal: controller.signal,
                });

                let recordResponse = response.data.data;
                setRoles(recordResponse);
            } catch (error) {
                if (error.response) {
                    setErrorMessage(error.response.data.message);
                }
            }
        };

        getRoles();

        const getRecord = async () => {
            try {
                setLoading(true);
                const response = await axiosPrivate.get(`/users/${params.id}`, {
                    signal: controller.signal,
                });

                let recordResponse = response.data.data;
                if (recordResponse) {
                    recordResponse._method = "put";
                }

                isMounted && setRecord(recordResponse);
            } catch (error) {
                if (error.response) {
                    setErrorMessage(error.response.data.message);
                }
            } finally {
                setLoading(false);
            }
        };

        if (params.id) {
            getRecord();
        } else {
            setRecord({
                id: null,
                username: "",
                role_id: undefined,
                active: 1,
                password: "",
                password_confirmation: "",
            });
        }

        return () => {
            isMounted = false;
            controller.abort();
        };
    }, []);

    return (
        <div>
            {loading && <div>Loading user ...</div>}

            {!loading && record && (
                <form className="form" onSubmit={handleSubmitForm} noValidate>
                    <SubtitleSection
                        title={params.id ? "Edit user" : "Create user"}
                    />

                    <div className="form__form-inline">
                        <div className="form__form-group">
                            <label className="form__label" htmlFor="name">
                                Username
                            </label>
                            <input
                                className="form__input"
                                id="name"
                                type="text"
                                value={record.username}
                                onChange={(e) => {
                                    setRecord({
                                        ...record,
                                        username: e?.target.value,
                                    });
                                }}
                            ></input>
                        </div>
                        <div className="form__form-group">
                            <label className="form__label" htmlFor="Status">
                                Role
                            </label>
                            <select
                                id="status"
                                className="form__select"
                                value={record.role_id}
                                onChange={(e) => {
                                    setRecord({
                                        ...record,
                                        role_id: e?.target.value,
                                    });
                                }}
                            >
                                <option value="">Select a role</option>
                                {roles &&
                                    roles.map((role) => (
                                        <option key={role.id} value={role.id}>
                                            {role.name}
                                        </option>
                                    ))}
                            </select>
                        </div>
                    </div>

                    <div className="form__form-inline">
                        <div className="form__form-group">
                            <label className="form__label" htmlFor="password">
                                Password
                            </label>
                            <input
                                className="form__input"
                                id="password"
                                type="password"
                                onChange={(e) => {
                                    setRecord({
                                        ...record,
                                        password: e?.target.value,
                                    });
                                }}
                            ></input>
                        </div>
                        <div className="form__form-group">
                            <label
                                className="form__label"
                                htmlFor="password_confirmation"
                            >
                                Confirm Password
                            </label>
                            <input
                                className="form__input"
                                id="password_confirmation"
                                type="password"
                                onChange={(e) => {
                                    setRecord({
                                        ...record,
                                        password_confirmation: e?.target.value,
                                    });
                                }}
                            ></input>
                        </div>
                    </div>

                    <div className="form__form-inline">
                        <div className="form__form-group">
                            <label className="form__label" htmlFor="Status">
                                Status
                            </label>
                            <select
                                id="status"
                                className="form__select"
                                value={record.active}
                                onChange={(e) => {
                                    setRecord({
                                        ...record,
                                        active: e?.target.value,
                                    });
                                }}
                            >
                                <option value="0">Inactive</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>

                    <button
                        type="submit"
                        className="btn btn--primary"
                        disabled={statusMessage !== null ? true : false}
                    >
                        {statusMessage !== null
                            ? statusMessage
                            : params.id
                            ? "Update"
                            : "Save"}
                    </button>
                    <button
                        className="btn btn--info mx-2"
                        onClick={(e) => {
                            e.preventDefault();
                            navigate("/users");
                        }}
                    >
                        Back
                    </button>
                </form>
            )}
            {errorMessage && <ErrorSection title={errorMessage} />}
        </div>
    );
}
