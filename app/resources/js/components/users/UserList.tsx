import React, { useEffect, useState } from "react";
import useAxiosPrivate from "@/hooks/useAxiosPrivate";
import { Link } from "react-router-dom";
import Pagination from "../common/Pagination";
import ErrorSection from "@/components/common/ErrorSection";
import { useAppSelector } from "@/hooks/reduxHooks";

type Role = {
    id: number;
    name: string;
};
type Record = {
    id: number;
    username: string;
    role: Role;
    active: number;
};

export default function UserList() {
    const auth = useAppSelector((state) => state.authState);
    const axiosPrivate = useAxiosPrivate();
    const [loading, setLoading] = useState(false);
    const [records, setRecords] = useState<Record[]>([]);
    const [errorMessage, setErrorMessage] = useState(null);

    const refreshRecords = async () => {
        const response = await axiosPrivate.get("/users");
        setRecords(response.data);
    };

    const handleDeleteRecord = async (id: number) => {
        if (window.confirm("Are you sure to delete this record?")) {
            await axiosPrivate.delete(`/users/${id}`);
            refreshRecords();
        }
    };

    const handleChangePage = async (page: string) => {
        if (page) {
            const pageParameters = page.split("?");
            const nextPage = pageParameters[1];
            const response = await axiosPrivate.get(`/users?${nextPage}`);
            setRecords(response.data);
        }
    };

    useEffect(() => {
        let isMounted = true;
        const controller = new AbortController();

        const getRecords = async () => {
            try {
                setLoading(true);
                const response = await axiosPrivate.get("/users", {
                    signal: controller.signal,
                });

                isMounted && setRecords(response.data);
            } catch (error) {
                if (error.response) {
                    setErrorMessage(error.response.data.message);
                }
            } finally {
                setLoading(false);
            }
        };
        getRecords();

        return () => {
            isMounted = false;
            controller.abort();
        };
    }, []);

    return (
        <>
            {loading && <div>Loading users ...</div>}
            {!loading && records.data && (
                <>
                    {auth.user && auth.user.role_id === 1 && (
                        <Link to="/users/create" className="btn btn--success">
                            New user
                        </Link>
                    )}

                    <table className="table-auto table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Active</th>
                                {auth.user && auth.user.role_id === 1 && (
                                    <th>Actions</th>
                                )}
                            </tr>
                        </thead>
                        <tbody>
                            {records.data.map((record) => (
                                <tr key={record.id}>
                                    <td>{record.id}</td>
                                    <td>{record.username}</td>
                                    <td>{record.role.name}</td>
                                    <td>
                                        {record.active ? "active" : "inactive"}
                                    </td>
                                    {auth.user && auth.user.role_id === 1 && (
                                        <td>
                                            <Link
                                                to={`/users/${record.id}/edit`}
                                                className="btn btn--info"
                                            >
                                                Edit
                                            </Link>

                                            <button
                                                onClick={() =>
                                                    handleDeleteRecord(
                                                        record.id
                                                    )
                                                }
                                                className="btn btn--danger"
                                            >
                                                Delete
                                            </button>
                                        </td>
                                    )}
                                </tr>
                            ))}
                        </tbody>
                    </table>

                    {!loading && records.pagination && (
                        <Pagination
                            links={records.pagination.links}
                            handleChangePage={handleChangePage}
                        />
                    )}

                    {errorMessage && <ErrorSection title={errorMessage} />}
                </>
            )}
        </>
    );
}
