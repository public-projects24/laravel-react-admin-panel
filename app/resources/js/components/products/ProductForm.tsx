import { useNavigate, useParams } from "react-router-dom";
import useAxiosPrivate from "@/hooks/useAxiosPrivate";
import React, { useState, useEffect } from "react";
import SubtitleSection from "@/components/common/SubtitleSection";
import ErrorSection from "@/components/common/ErrorSection";
import { toast } from "sonner";

type Category = {
    id: number;
    name: string;
};

type Subcategory = {
    id: number;
    name: string;
    active: boolean;
    category_id: number;
};

type Record = {
    id: number;
    subcategory_id: number | undefined;
    name: string;
    active: boolean;
};

export default function SubcategoryForm() {
    let params = useParams();
    const axiosPrivate = useAxiosPrivate();
    const navigate = useNavigate();
    const [categories, setCategories] = useState<Category[]>();
    const [subcategories, setSubcategories] = useState<Subcategory[]>();
    const [record, setRecord] = useState<Record>();
    const [loading, setLoading] = useState(false);
    const [statusMessage, setStatusMessage] = useState(null);
    const [errorMessage, setErrorMessage] = useState(null);

    const handleChangeCategory = async (e: React.SyntheticEvent) => {
        const categoryId = e.target.value;
        if (categoryId) {
            try {
                const response = await axiosPrivate.get(
                    `/subcategories?category=${categoryId}`
                );

                const recordResponse = response.data.data;
                setSubcategories(recordResponse);
            } catch (error) {
                if (error.response) {
                    toast.error(error.response.data.message);
                }
            }
        }
    };

    const handleSubmitForm = async (e: React.SyntheticEvent) => {
        e.preventDefault();

        setStatusMessage(params.id ? "Updating ..." : "Creating ...");

        try {
            let response = null;
            if (params.id) {
                response = await axiosPrivate.post(
                    `/products/${record.id}`,
                    record
                );
            } else {
                response = await axiosPrivate.post(`/products`, record);
            }

            toast.success(response.data.message);
        } catch (error) {
            toast.error(error.response.data.message);
        } finally {
            setStatusMessage(null);
        }
    };

    useEffect(() => {
        let isMounted = true;
        const controller = new AbortController();

        const getCategories = async () => {
            try {
                const response = await axiosPrivate.get(
                    `/categories?getAll=true`,
                    {
                        signal: controller.signal,
                    }
                );

                let recordResponse = response.data.data;
                isMounted && setCategories(recordResponse);
            } catch (error) {
                if (error.response) {
                    setErrorMessage(error.response.data.message);
                }
            }
        };

        getCategories();

        const getSubcategory = async (id: number) => {
            try {
                const response = await axiosPrivate.get(`/subcategories/${id}`);
                const recordResponse = response.data.data;
                setSubcategories([recordResponse]);
            } catch (error) {
                if (error.response) {
                    setErrorMessage(error.response.data.message);
                }
            }
        };

        const getRecord = async () => {
            try {
                setLoading(true);
                const response = await axiosPrivate.get(
                    `/products/${params.id}`,
                    {
                        signal: controller.signal,
                    }
                );

                let recordResponse = response.data.data;
                if (recordResponse) {
                    recordResponse._method = "put";
                }

                isMounted && setRecord(recordResponse);
                getSubcategory(recordResponse.subcategory_id);
            } catch (error) {
                if (error.response) {
                    setErrorMessage(error.response.data.message);
                }
            } finally {
                setLoading(false);
            }
        };

        if (params.id) {
            getRecord();
        } else {
            setRecord({
                id: null,
                category_id: undefined,
                name: "",
                active: 1,
            });
        }

        return () => {
            isMounted = false;
            controller.abort();
        };
    }, []);

    return (
        <div>
            {loading && <div>Loading subcategory ...</div>}

            {!loading && record && (
                <form className="form" onSubmit={handleSubmitForm} noValidate>
                    <SubtitleSection
                        title={
                            params.id
                                ? "Edit subcategory"
                                : "Create subcategory"
                        }
                    />

                    <div className="form__form-inline">
                        <div className="form__form-group">
                            <label className="form__label" htmlFor="Status">
                                Category
                            </label>
                            <select
                                id="category"
                                className="form__select"
                                onChange={handleChangeCategory}
                            >
                                <option value="">Select a category</option>
                                {categories &&
                                    categories.map((category) => (
                                        <option
                                            value={category.id}
                                            key={category.id}
                                        >
                                            {category.name}
                                        </option>
                                    ))}
                            </select>
                        </div>

                        <div className="form__form-group">
                            <label className="form__label" htmlFor="Status">
                                Subcategory
                            </label>
                            <select
                                id="category"
                                className="form__select"
                                value={record.subcategory_id}
                                onChange={(e) => {
                                    setRecord({
                                        ...record,
                                        subcategory_id: e?.target.value,
                                    });
                                }}
                            >
                                <option value="">Select a subcategory</option>
                                {subcategories &&
                                    subcategories.map((subcategory) => (
                                        <option
                                            value={subcategory.id}
                                            key={subcategory.id}
                                        >
                                            {subcategory.name}
                                        </option>
                                    ))}
                            </select>
                        </div>
                        <div className="form__form-group">
                            <label className="form__label" htmlFor="name">
                                Name
                            </label>
                            <input
                                className="form__input"
                                id="name"
                                type="text"
                                value={record.name}
                                onChange={(e) => {
                                    setRecord({
                                        ...record,
                                        name: e?.target.value,
                                    });
                                }}
                            ></input>
                        </div>
                    </div>

                    <button
                        className="btn btn--primary"
                        disabled={statusMessage !== null ? true : false}
                    >
                        {statusMessage !== null
                            ? statusMessage
                            : params.id
                            ? "Update"
                            : "Save"}
                    </button>
                    <button
                        className="btn btn--info mx-2"
                        onClick={(e) => {
                            e.preventDefault();
                            navigate("/products");
                        }}
                    >
                        Back
                    </button>
                </form>
            )}
            {errorMessage && <ErrorSection title={errorMessage} />}
        </div>
    );
}
