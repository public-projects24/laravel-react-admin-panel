import React, { useEffect, useState } from "react";
import useAxiosPrivate from "@/hooks/useAxiosPrivate";
import { Link } from "react-router-dom";
import Pagination from "../common/Pagination";
import ErrorSection from "@/components/common/ErrorSection";
import { useAppSelector } from "@/hooks/reduxHooks";

type Record = {
    id: number;
    name: string;
    status: string;
};

export default function ProductList() {
    const auth = useAppSelector((state) => state.authState);
    const axiosPrivate = useAxiosPrivate();
    const [loading, setLoading] = useState(false);
    const [records, setRecords] = useState<Record[]>([]);
    const [errorMessage, setErrorMessage] = useState(null);

    const refreshRecords = async () => {
        const response = await axiosPrivate.get("/products");
        setRecords(response.data);
    };

    const handleDeleteRecord = async (id: number) => {
        if (window.confirm("Are you sure to delete this record?")) {
            await axiosPrivate.delete(`/products/${id}`);
            refreshRecords();
        }
    };

    const handleChangePage = async (page: string) => {
        if (page) {
            const pageParameters = page.split("?");
            const nextPage = pageParameters[1];
            const response = await axiosPrivate.get(`/products?${nextPage}`);
            setRecords(response.data);
        }
    };

    useEffect(() => {
        let isMounted = true;
        const controller = new AbortController();

        const getRecords = async () => {
            try {
                setLoading(true);
                const response = await axiosPrivate.get("/products", {
                    signal: controller.signal,
                });

                isMounted && setRecords(response.data);
            } catch (error) {
                if (error.response) {
                    setErrorMessage(error.response.data.message);
                }
            } finally {
                setLoading(false);
            }
        };
        getRecords();

        return () => {
            isMounted = false;
            controller.abort();
        };
    }, []);

    return (
        <>
            {loading && <div>Loading products ...</div>}
            {!loading && records.data && (
                <>
                    {auth.user && auth.user.role_id === 1 && (
                        <Link
                            to="/products/create"
                            className="btn btn--success"
                        >
                            New product
                        </Link>
                    )}

                    <table className="table-auto table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Status</th>
                                {auth.user && auth.user.role_id === 1 && (
                                    <th>Actions</th>
                                )}
                            </tr>
                        </thead>
                        <tbody>
                            {records.data.map((record) => (
                                <tr key={record.id}>
                                    <td>{record.id}</td>
                                    <td>{record.name}</td>
                                    <td>{record.status}</td>
                                    {auth.user && auth.user.role_id === 1 && (
                                        <td>
                                            <Link
                                                to={`/products/${record.id}/edit`}
                                                className="btn btn--info"
                                            >
                                                Edit
                                            </Link>

                                            <button
                                                onClick={() =>
                                                    handleDeleteRecord(
                                                        record.id
                                                    )
                                                }
                                                className="btn btn--danger"
                                            >
                                                Delete
                                            </button>
                                        </td>
                                    )}
                                </tr>
                            ))}
                        </tbody>
                    </table>

                    {!loading && records.meta && (
                        <Pagination
                            links={records.meta.links}
                            handleChangePage={handleChangePage}
                        />
                    )}

                    {errorMessage && <ErrorSection title={errorMessage} />}
                </>
            )}
        </>
    );
}
