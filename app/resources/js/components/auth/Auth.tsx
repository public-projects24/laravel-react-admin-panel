import "@/assets/css/components/auth/auth.css";
import TitleSection from "../common/TitleSection";
import React, { useState, useEffect } from "react";

import { useAppDispatch, useAppSelector } from "@/hooks/reduxHooks";
import { loginRequest, clearErrorMessage } from "@/store/authSlice";

import { useLocation, useNavigate } from "react-router-dom";

export default function Auth() {
    const dispatch = useAppDispatch();
    const auth = useAppSelector((state) => state.authState);

    const navigate = useNavigate();
    const location = useLocation();
    const from = location.state?.from?.pathname || "/";
    const [credentials, setCredentials] = useState({
        username: "admin",
        password: "123456",
    });

    const onSubmitForm = async (e: React.SyntheticEvent) => {
        e.preventDefault();

        // Form 1
        dispatch(clearErrorMessage());
        await dispatch(loginRequest(credentials));
    };

    useEffect(() => {
        if (auth.isAuthenticated) {
            navigate(from, { replace: true });
        }
    }, [auth.isAuthenticated]);

    return (
        <div className="auth container mx-auto">
            <TitleSection title="Sign in" />
            <form className="form" onSubmit={onSubmitForm} noValidate>
                <div className="form__form-group">
                    <label className="form__label" htmlFor="username">
                        Username
                    </label>
                    <input
                        className="form__input"
                        id="username"
                        type="text"
                        placeholder="johndoe@example.com"
                        value={credentials.username}
                        onChange={(e) => {
                            setCredentials({
                                ...credentials,
                                username: e?.target.value,
                            });
                        }}
                    ></input>
                </div>
                <div className="form__form-group">
                    <label className="form__label" htmlFor="password">
                        Password
                    </label>
                    <input
                        className="form__input"
                        id="password"
                        type="password"
                        placeholder="johndoe@example.com"
                        value={credentials.password}
                        onChange={(e) => {
                            setCredentials({
                                ...credentials,
                                password: e?.target.value,
                            });
                        }}
                    ></input>
                </div>

                {auth.errorMessage && (
                    <div className="form-group form__error-message">
                        {auth.errorMessage}
                    </div>
                )}
                <button
                    className="btn btn--primary auth__button"
                    disabled={auth.statusMessage !== null ? true : false}
                >
                    {auth.statusMessage !== null
                        ? auth.statusMessage
                        : "Sign in"}
                </button>
            </form>
        </div>
    );
}
