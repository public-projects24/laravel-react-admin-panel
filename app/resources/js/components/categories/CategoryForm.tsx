import { useNavigate, useParams } from "react-router-dom";
import useAxiosPrivate from "@/hooks/useAxiosPrivate";
import React, { useState, useEffect } from "react";
import SubtitleSection from "@/components/common/SubtitleSection";
import ErrorSection from "@/components/common/ErrorSection";
import { toast } from "sonner";

type Record = {
    id: number;
    name: string;
    active: boolean;
};

export default function CategoryForm() {
    let params = useParams();
    const axiosPrivate = useAxiosPrivate();
    const navigate = useNavigate();
    const [record, setRecord] = useState<Record>();
    const [loading, setLoading] = useState(false);
    const [statusMessage, setStatusMessage] = useState(null);
    const [errorMessage, setErrorMessage] = useState(null);

    const handleSubmitForm = async (e: React.SyntheticEvent) => {
        e.preventDefault();

        setStatusMessage(params.id ? "Updating ..." : "Creating ...");

        try {
            let response = null;
            if (params.id) {
                response = await axiosPrivate.post(
                    `/categories/${record.id}`,
                    record
                );
            } else {
                response = await axiosPrivate.post(`/categories`, record);
            }

            toast.success(response.data.message);
        } catch (error) {
            toast.error(error.response.data.message);
        } finally {
            setStatusMessage(null);
        }
    };

    useEffect(() => {
        let isMounted = true;
        const controller = new AbortController();

        const getRecord = async () => {
            try {
                setLoading(true);
                const response = await axiosPrivate.get(
                    `/categories/${params.id}`,
                    {
                        signal: controller.signal,
                    }
                );

                let recordResponse = response.data.data;
                if (recordResponse) {
                    recordResponse._method = "put";
                }

                isMounted && setRecord(recordResponse);
            } catch (error) {
                if (error.response) {
                    setErrorMessage(error.response.data.message);
                }
            } finally {
                setLoading(false);
            }
        };

        if (params.id) {
            getRecord();
        } else {
            setRecord({
                id: null,
                name: "",
                active: 1,
            });
        }

        return () => {
            isMounted = false;
            controller.abort();
        };
    }, []);

    return (
        <div>
            {loading && <div>Loading category ...</div>}

            {!loading && record && (
                <form className="form" onSubmit={handleSubmitForm} noValidate>
                    <SubtitleSection
                        title={params.id ? "Edit category" : "Create category"}
                    />

                    <div className="form__form-inline">
                        <div className="form__form-group">
                            <label className="form__label" htmlFor="name">
                                Name
                            </label>
                            <input
                                className="form__input"
                                id="name"
                                type="text"
                                value={record.name}
                                onChange={(e) => {
                                    setRecord({
                                        ...record,
                                        name: e?.target.value,
                                    });
                                }}
                            ></input>
                        </div>
                        <div className="form__form-group">
                            <label className="form__label" htmlFor="Status">
                                Status
                            </label>
                            <select
                                id="status"
                                className="form__select"
                                value={record.active}
                                onChange={(e) => {
                                    setRecord({
                                        ...record,
                                        active: e?.target.value,
                                    });
                                }}
                            >
                                <option value="0">Inactive</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>

                    <button
                        type="submit"
                        className="btn btn--primary"
                        disabled={statusMessage !== null ? true : false}
                    >
                        {statusMessage !== null
                            ? statusMessage
                            : params.id
                            ? "Update"
                            : "Save"}
                    </button>
                    <button
                        className="btn btn--info mx-2"
                        onClick={(e) => {
                            e.preventDefault();
                            navigate("/categories");
                        }}
                    >
                        Back
                    </button>
                </form>
            )}
            {errorMessage && <ErrorSection title={errorMessage} />}
        </div>
    );
}
