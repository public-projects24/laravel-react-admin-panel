import { ReactNode } from "react";
import { Navigate } from "react-router-dom";

import { useAppSelector } from "@/hooks/reduxHooks";

type ProtectedGuestRouteProps = {
    children: ReactNode;
};
export default function ProtectedRoute(props: ProtectedGuestRouteProps) {
    const auth = useAppSelector((state) => state.authState);

    if (auth.isAuthenticated) {
        // user is authenticated
        return <Navigate to="/profile" />;
    }
    return props.children;
}
