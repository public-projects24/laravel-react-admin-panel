import { ReactNode } from "react";
import { useAppSelector } from "@/hooks/reduxHooks";
import React from "react";
import ErrorSection from "@/components/common/ErrorSection";

type ProtectedAdminRouteProps = {
    children: ReactNode;
};
export default function ProtectedAdminRoute(props: ProtectedAdminRouteProps) {
    const auth = useAppSelector((state) => state.authState);

    if (auth.user && auth.user.role_id !== 1) {
        // user is not admin
        return <ErrorSection title="Forbidden Access" />;
    }
    return props.children;
}
