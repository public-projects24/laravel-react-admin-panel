import "@/assets/css/components/common/subtitle-section.css";

type SubtitleSectionProps = {
    title: string;
};
const SubtitleSection = (props: SubtitleSectionProps) => {
    return (
        <>
            <div className="subtitle-section">
                <h2 className="subtitle-section__subtitle">{props.title}</h2>
            </div>
        </>
    );
};

export default SubtitleSection;
