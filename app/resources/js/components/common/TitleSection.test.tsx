import TitleSection from "./TitleSection";
import { render, screen } from "@testing-library/react";

describe("TitleSection", () => {
    test("component can be rendered", () => {
        const title = "my title";
        render(<TitleSection title={title} />);
        expect(screen.getByText(title)).toBeDefined();
    });
});
