import "@/assets/css/components/common/error-section.css";

type ErrorSectionProps = {
    title: string;
};
const ErrorSection = (props: ErrorSectionProps) => {
    return (
        <>
            <div className="error-section">
                <h2 className="error-section__title">{props.title}</h2>
            </div>
        </>
    );
};

export default ErrorSection;
