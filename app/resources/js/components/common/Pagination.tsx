import "@/assets/css/components/common/pagination.css";

type Links = {
    url: string | null;
    label: string;
    active: boolean;
};

type Props = {
    links: Array<Links>;
    handleChangePage: (page: string) => void;
};

export default function Pagination(props: Props) {
    if (props.links.length > 3) {
        return (
            <ul className="pagination">
                {props.links.map((lin, index) => (
                    <li
                        className={`pagination__item ${
                            lin.active ? "pagination__item--active" : ""
                        }`}
                        key={index}
                        onClick={() => {
                            props.handleChangePage(lin.url);
                        }}
                    >
                        {index === 0 ? "Prev" : ""}
                        {index > 0 && index < props.links.length - 1
                            ? lin.label
                            : ""}
                        {index === props.links.length - 1 ? "Next" : ""}
                    </li>
                ))}
            </ul>
        );
    }

    return null;
}
