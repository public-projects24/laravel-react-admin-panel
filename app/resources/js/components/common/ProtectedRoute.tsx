import { ReactNode } from "react";
import { Navigate, useLocation } from "react-router-dom";

import { useAppSelector, useAppDispatch } from "@/hooks/reduxHooks";
import { setUser } from "@/store/authSlice";
import React, { useEffect } from "react";
import useAxiosPrivate from "@/hooks/useAxiosPrivate";

type ProtectedRouteProps = {
    children: ReactNode;
};
export default function ProtectedRoute(props: ProtectedRouteProps) {
    const auth = useAppSelector((state) => state.authState);
    const dispatch = useAppDispatch();
    const location = useLocation();
    const axiosPrivate = useAxiosPrivate();

    useEffect(() => {
        let isMounted = true;
        const controller = new AbortController();

        const getUser = async () => {
            const response = await axiosPrivate.get("/profile", {
                signal: controller.signal,
            });

            let userResponse = response.data.data;
            isMounted && dispatch(setUser(userResponse));
        };
        getUser();

        return () => {
            isMounted = false;
            controller.abort();
        };
    }, []);

    if (!auth.isAuthenticated) {
        // user is not authenticated
        return <Navigate to="/" state={{ from: location }} replace />;
    }
    return props.children;
}
