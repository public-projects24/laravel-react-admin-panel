import "@/assets/css/components/common/navmenu.css";

import { logoutRequest } from "@/store/authSlice";
import { useAppDispatch, useAppSelector } from "@/hooks/reduxHooks";

import { NavLink, Link } from "react-router-dom";
import { MdMenu } from "react-icons/md";
import { useState } from "react";

const NavMenu = () => {
    const auth = useAppSelector((state) => state.authState);
    const dispatch = useAppDispatch();
    const [showMobileMenu, setShowMobileMenu] = useState(false);

    const handleToggleMobileMenu = () => {
        setShowMobileMenu(!showMobileMenu);
    };
    const handleCloseMenu = () => {
        setShowMobileMenu(false);
    };

    return (
        <nav className="navmenu">
            <div className="navmenu__container container mx-auto">
                <div className="navmenu__logo">
                    <Link onClick={handleCloseMenu} to="/">
                        <img
                            src="/img/common/logo-mobile.png"
                            alt="logo"
                            className="navmenu__logo-img lg:hidden"
                        />
                    </Link>
                    <Link onClick={handleCloseMenu} to="/">
                        <img
                            src="/img/common/logo-dark.png"
                            alt="logo"
                            className="navmenu__logo-img hidden lg:block"
                        />
                    </Link>
                </div>
                <div
                    className={`navmenu__collapsable ${
                        showMobileMenu ? "navmenu__collapsable--active" : ""
                    }`}
                >
                    {auth.isAuthenticated && (
                        <ul className="navmenu__list">
                            <li className="navmenu__list-item">
                                <NavLink
                                    onClick={handleCloseMenu}
                                    to="/profile"
                                    className={({ isActive }) =>
                                        isActive
                                            ? `navmenu__list-link navmenu__list-link--active`
                                            : `navmenu__list-link`
                                    }
                                >
                                    Profile
                                </NavLink>
                            </li>
                            <li className="navmenu__list-item">
                                <NavLink
                                    onClick={handleCloseMenu}
                                    to="/categories"
                                    className={({ isActive }) =>
                                        isActive
                                            ? `navmenu__list-link navmenu__list-link--active`
                                            : `navmenu__list-link`
                                    }
                                >
                                    Categories
                                </NavLink>
                            </li>
                            <li className="navmenu__list-item">
                                <NavLink
                                    onClick={handleCloseMenu}
                                    to="/subcategories"
                                    className={({ isActive }) =>
                                        isActive
                                            ? `navmenu__list-link navmenu__list-link--active`
                                            : `navmenu__list-link`
                                    }
                                >
                                    Subcategories
                                </NavLink>
                            </li>
                            <li className="navmenu__list-item">
                                <NavLink
                                    onClick={handleCloseMenu}
                                    to="/products"
                                    className={({ isActive }) =>
                                        isActive
                                            ? `navmenu__list-link navmenu__list-link--active`
                                            : `navmenu__list-link`
                                    }
                                >
                                    Products
                                </NavLink>
                            </li>
                            <li className="navmenu__list-item">
                                <NavLink
                                    onClick={handleCloseMenu}
                                    to="/users"
                                    className={({ isActive }) =>
                                        isActive
                                            ? `navmenu__list-link navmenu__list-link--active`
                                            : `navmenu__list-link`
                                    }
                                >
                                    Users
                                </NavLink>
                            </li>

                            <li className="navmenu__list-item lg:hidden">
                                {auth.isAuthenticated && (
                                    <>
                                        <button
                                            className="btn btn--outline lg:hidden"
                                            onClick={() => {
                                                dispatch(logoutRequest());
                                            }}
                                        >
                                            Log out
                                        </button>
                                    </>
                                )}
                            </li>
                        </ul>
                    )}
                </div>
                <div className="navmenu__buttons">
                    {auth.isAuthenticated && (
                        <button
                            className="navmenu__cart-hamburger lg:hidden"
                            onClick={handleToggleMobileMenu}
                        >
                            <MdMenu />
                        </button>
                    )}

                    {auth.isAuthenticated && (
                        <>
                            <button
                                className="btn btn--outline hidden lg:block"
                                onClick={() => {
                                    dispatch(logoutRequest());
                                }}
                            >
                                Log out
                            </button>
                        </>
                    )}
                </div>
            </div>
        </nav>
    );
};

export default NavMenu;
