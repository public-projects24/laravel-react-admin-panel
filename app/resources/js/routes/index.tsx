import { createBrowserRouter } from "react-router-dom";
import { lazy } from "react";
import Default from "@/layouts/Default";

const ErrorPage = lazy(() => import("@/pages/ErrorPage"));

const ProfilePage = lazy(() => import("@/pages/ProfilePage"));
const CategoriesPage = lazy(() => import("@/pages/CategoriesPage"));
const CategoryList = lazy(() => import("@/components/categories/CategoryList"));
const CategoryForm = lazy(() => import("@/components/categories/CategoryForm"));

const SubcategoriesPage = lazy(() => import("@/pages/SubcategoriesPage"));
const SubcategoryList = lazy(
    () => import("@/components/subcategories/SubcategoryList")
);
const SubcategoryForm = lazy(
    () => import("@/components/subcategories/SubcategoryForm")
);

const ProductsPage = lazy(() => import("@/pages/ProductsPage"));
const ProductList = lazy(() => import("@/components/products/ProductList"));
const ProductForm = lazy(() => import("@/components/products/ProductForm"));

const UsersPage = lazy(() => import("@/pages/UsersPage"));
const UserList = lazy(() => import("@/components/users/UserList"));
const UserForm = lazy(() => import("@/components/users/UserForm"));

const SigInPage = lazy(() => import("@/pages/SignInPage"));
const ProtectedRoute = lazy(() => import("@/components/common/ProtectedRoute"));
const ProtectedAdminRoute = lazy(
    () => import("@/components/common/ProtectedAdminRoute")
);
const ProtectedGuestRoute = lazy(
    () => import("@/components/common/ProtectedGuestRoute")
);

export const router = createBrowserRouter([
    {
        path: "/",
        element: <Default />,
        errorElement: <ErrorPage />,
        children: [
            {
                path: "/",
                element: (
                    <ProtectedGuestRoute>
                        <SigInPage />
                    </ProtectedGuestRoute>
                ),
            },
            {
                path: "profile",
                element: (
                    <ProtectedRoute>
                        <ProfilePage />
                    </ProtectedRoute>
                ),
            },
            {
                path: "categories",
                element: (
                    <ProtectedRoute>
                        <CategoriesPage />
                    </ProtectedRoute>
                ),
                children: [
                    {
                        path: "",
                        element: <CategoryList />,
                    },
                    {
                        path: "/categories/create",
                        element: (
                            <ProtectedAdminRoute>
                                <CategoryForm />
                            </ProtectedAdminRoute>
                        ),
                    },
                    {
                        path: "/categories/:id/edit",
                        element: (
                            <ProtectedAdminRoute>
                                <CategoryForm />
                            </ProtectedAdminRoute>
                        ),
                    },
                ],
            },
            {
                path: "subcategories",
                element: (
                    <ProtectedRoute>
                        <SubcategoriesPage />
                    </ProtectedRoute>
                ),
                children: [
                    {
                        path: "",
                        element: <SubcategoryList />,
                    },
                    {
                        path: "/subcategories/create",
                        element: (
                            <ProtectedAdminRoute>
                                <SubcategoryForm />
                            </ProtectedAdminRoute>
                        ),
                    },
                    {
                        path: "/subcategories/:id/edit",
                        element: (
                            <ProtectedAdminRoute>
                                <SubcategoryForm />
                            </ProtectedAdminRoute>
                        ),
                    },
                ],
            },
            {
                path: "products",
                element: (
                    <ProtectedRoute>
                        <ProductsPage />
                    </ProtectedRoute>
                ),
                children: [
                    {
                        path: "",
                        element: <ProductList />,
                    },
                    {
                        path: "/products/create",
                        element: (
                            <ProtectedAdminRoute>
                                <ProductForm />
                            </ProtectedAdminRoute>
                        ),
                    },
                    {
                        path: "/products/:id/edit",
                        element: (
                            <ProtectedAdminRoute>
                                <ProductForm />
                            </ProtectedAdminRoute>
                        ),
                    },
                ],
            },
            {
                path: "users",
                element: (
                    <ProtectedRoute>
                        <UsersPage />
                    </ProtectedRoute>
                ),
                children: [
                    {
                        path: "",
                        element: <UserList />,
                    },
                    {
                        path: "/users/create",
                        element: (
                            <ProtectedAdminRoute>
                                <UserForm />
                            </ProtectedAdminRoute>
                        ),
                    },
                    {
                        path: "/users/:id/edit",
                        element: (
                            <ProtectedAdminRoute>
                                <UserForm />
                            </ProtectedAdminRoute>
                        ),
                    },
                ],
            },
        ],
    },
]);
