/** @type {import('tailwindcss').Config} */
export default {
    content: ["./resources/js/**/*.{js,ts,jsx,tsx}"],
    theme: {
        extend: {
            colors: {
                primary: "#fe696a",
                secondary: "#f3f5f9",
                danger: "#f34770",
                succes: "#42d697",
                info: "#69b3fe",
            },
            fontFamily: {
                body: ["Rubik"],
            },
            spacing: {
                11.5: "2.875rem",
                15: "3.75rem",
                125: "31.25rem",
            },
        },
        container: {
            padding: {
                DEFAULT: "1.25rem",
                sm: "0",
                lg: "0",
                xl: "0",
                "2xl": "0",
            },
        },
    },
    plugins: [],
};
