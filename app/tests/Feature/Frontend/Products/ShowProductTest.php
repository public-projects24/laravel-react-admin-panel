<?php

namespace Tests\Feature\Frontend\Products;

use App\Models\Product;
use App\Models\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

use function PHPUnit\Framework\assertEquals;

class ShowProductTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    public function test_admin_user_can_view_product(): void
    {        
        $adminUser = User::where('id', 1)->first();
        $token = $adminUser->createToken('auth-token')->plainTextToken;
        $product = Product::where('id', 1)->first();

 
        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->getJson('/api/products/' . $product->id, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['data']);
            }
        );
    }

    public function test_basic_user_can_view_product(): void
    {        
        $basicUser = User::where('id', 2)->first();
        $token = $basicUser->createToken('auth-token')->plainTextToken;
        $product = Product::where('id', 1)->first();

 
        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->getJson('/api/products/' . $product->id, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['data']);
            }
        );
    }
}
