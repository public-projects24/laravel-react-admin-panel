<?php

namespace Tests\Feature\Frontend\Products;

use App\Models\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    public function test_admin_user_can_create_product(): void
    {
        $faker = Factory::create();
        
        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $data = [
            'name' => 'prod 1',
            'subcategory_id' => 1
        ];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->postJson('/api/products', $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_CREATED)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['message','product']);
            }
        );

        $this->assertDatabaseHas('products', $data);
    }

   public function test_admin_user_cannot_create_product_without_data(): void
    {        
        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->postJson('/api/products', [], $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function test_basic_user_cannot_create_product(): void
    {        
        $user = User::where('id', 2)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $data = [
            'name' => 'prod 1',
            'subcategory_id' => 1
        ];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->postJson('/api/products', $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
