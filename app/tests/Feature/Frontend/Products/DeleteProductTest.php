<?php

namespace Tests\Feature\Frontend\Products;

use App\Models\Product;
use App\Models\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

use function PHPUnit\Framework\assertEquals;

class DeleteProductTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    public function test_admin_user_can_delete_product(): void
    {        
        $adminUser = User::where('id', 1)->first();
        $token = $adminUser->createToken('auth-token')->plainTextToken;
        $product = Product::where('id', 1)->first();

        $data = [];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->deleteJson('/api/products/' . $product->id, $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['message','product']);
            }
        );

        $this->assertModelMissing($product);
       
    }

   public function test_basic_user_cannot_delete_product(): void
    {        
        $basicUser = User::where('id', 2)->first();
        $product = Product::where('id', 1)->first();
        $token = $basicUser->createToken('auth-token')->plainTextToken;
        
        $data = [];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->deleteJson('/api/products/' . $product->id, $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        
        
        $this->assertModelExists($product);
    } 
}
