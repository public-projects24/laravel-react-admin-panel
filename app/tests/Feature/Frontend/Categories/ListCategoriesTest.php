<?php

namespace Tests\Feature\Frontend\Categories;

use App\Models\Role;
use App\Models\User;
use Database\Seeders\UserSeeder;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class ListCategoriesTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    public function test_admin_user_can_list_categories(): void
    {        
        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->getJson('/api/categories', $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['data']);
            }
        );
    }

    public function test_basic_user_can_list_categories(): void
    {
        $faker = Factory::create();
        
        $user = User::where('id', 2)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->getJson('/api/categories', $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['data']);
            }
        );
    }
}
