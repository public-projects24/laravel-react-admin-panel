<?php

namespace Tests\Feature\Frontend\Subcategories;

use App\Models\Category;
use App\Models\Subcategory;
use App\Models\User;
use Database\Seeders\UserSeeder;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

use function PHPUnit\Framework\assertEquals;

class UpdateSubcategoryTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    public function test_admin_user_can_update_subcategory(): void
    {        
        $adminUser = User::where('id', 1)->first();
        $token = $adminUser->createToken('auth-token')->plainTextToken;
        $subcategory = Subcategory::where('id', 1)->first();

        $data = [
            'name' => 'subcat 1 mod',
            'active' => 0,
            'category_id' => 2,
        ];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->putJson('/api/subcategories/' . $subcategory->id, $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['message','subcategory']);
            }
        );
        
        $this->assertDatabaseHas('subcategories',$data);
    }


    public function test_admin_user_cannot_update_user_whithout_data(): void
    {
        $adminUser = User::where('id', 1)->first();
        $token = $adminUser->createToken('auth-token')->plainTextToken;
        $subcategory = Subcategory::where('id', 1)->first();

        $data = [];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->putJson('/api/subcategories/' . $subcategory->id, $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function test_basic_user_cannot_update_subcategory(): void
    {
        $basicUser = User::where('id', 2)->first();
        $token = $basicUser->createToken('auth-token')->plainTextToken;
        $subcategory = Subcategory::where('id', 1)->first();

        $data = [
            'name' => 'cat 1',
            'active' => 0,
            'category_id' => 2,
        ];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->putJson('/api/subcategories/' . $subcategory->id, $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    } 
}
