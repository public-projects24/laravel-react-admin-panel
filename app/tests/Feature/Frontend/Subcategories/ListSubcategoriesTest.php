<?php

namespace Tests\Feature\Frontend\Subcategories;

use App\Models\Role;
use App\Models\User;
use Database\Seeders\UserSeeder;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class ListSubcategoriesTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    public function test_admin_user_can_list_subcategories(): void
    {        
        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->getJson('/api/subcategories', $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['data']);
            }
        );
    }

    public function test_admin_user_can_list_subcategories_by_category(): void
    {        
        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $categoryId = 1;
        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->getJson('/api/subcategories?category=' . $categoryId, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['data']);
            }
        );

        $this->assertEquals($response['data'][0]['category_id'], $categoryId);
    }

    public function test_basic_user_can_list_subcategories(): void
    {
        $faker = Factory::create();
        
        $user = User::where('id', 2)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->getJson('/api/subcategories', $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['data']);
            }
        );
    }

    public function test_basic_user_can_list_subcategories_by_category(): void
    {        
        $user = User::where('id', 2)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $categoryId = 1;
        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->getJson('/api/subcategories?category=' . $categoryId, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['data']);
            }
        );

        $this->assertEquals($response['data'][0]['category_id'], $categoryId);
    }
}
