<?php

namespace Tests\Feature\Frontend\Subcategories;

use App\Models\Category;
use App\Models\Role;
use App\Models\Subcategory;
use App\Models\User;
use Database\Seeders\UserSeeder;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

use function PHPUnit\Framework\assertEquals;

class DeleteSubcategoryTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    public function test_admin_user_can_delete_subcategory(): void
    {        
        $adminUser = User::where('id', 1)->first();
        $token = $adminUser->createToken('auth-token')->plainTextToken;
        $subcategory = Subcategory::where('id', 1)->first();

        $data = [];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->deleteJson('/api/subcategories/' . $subcategory->id, $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['message','subcategory']);
            }
        );

        $this->assertModelMissing($subcategory);
       
    }

    public function test_basic_user_cannot_delete_subcategory(): void
    {        
        $basicUser = User::where('id', 2)->first();
        $subcategory = Subcategory::where('id', 1)->first();
        $token = $basicUser->createToken('auth-token')->plainTextToken;
        
        $data = [];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->deleteJson('/api/subcategories/' . $subcategory->id, $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        
        
        $this->assertModelExists($subcategory);
    }  
}
