<?php

namespace Tests\Feature\Frontend\Subcategories;

use App\Models\Role;
use App\Models\User;
use Database\Seeders\UserSeeder;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateSubcategoryTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    public function test_admin_user_can_create_subcategory(): void
    {
        $faker = Factory::create();
        
        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $data = [
            'name' => 'subcat 1',
            'active' => 1,
            'category_id' => 1
        ];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->postJson('/api/subcategories', $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_CREATED)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['message','subcategory']);
            }
        );

        $this->assertDatabaseHas('subcategories', [
            'name' => 'subcat 1',
            'active' => 1,
            'category_id' => 1
        ]);
    }

   public function test_admin_user_cannot_create_category_without_data(): void
    {        
        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->postJson('/api/subcategories', [], $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function test_basic_user_cannot_create_subcategory(): void
    {
        $faker = Factory::create();
        
        $user = User::where('id', 2)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $data = [
            'name' => 'subcat 1',
            'active' => 1,
            'category_id' => 1
        ];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->postJson('/api/subcategories', $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
