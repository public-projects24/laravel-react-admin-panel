<?php

namespace Tests\Feature\Frontend\Users;

use App\Models\Role;
use App\Models\User;
use Database\Seeders\UserSeeder;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

use function PHPUnit\Framework\assertEquals;

class DeleteUserTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    public function test_admin_user_can_delete_user(): void
    {        
        $adminUser = User::where('id', 1)->first();
        $token = $adminUser->createToken('auth-token')->plainTextToken;
        $basicUser = User::where('id', 2)->first();

        $data = [];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->deleteJson('/api/users/' . $basicUser->id, $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['message','user']);
            }
        );

        $this->assertModelMissing($basicUser);
       
    }

   public function test_basic_user_cannot_delete_user(): void
    {
        $faker = Factory::create();
        
        $adminUser = User::where('id', 1)->first();
        $basicUser = User::where('id', 2)->first();
        $token = $basicUser->createToken('auth-token')->plainTextToken;
        
        $data = [];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->deleteJson('/api/users/' . $adminUser->id, $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        
        
        $this->assertModelExists($adminUser);
    } 
}
