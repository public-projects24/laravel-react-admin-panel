<?php

namespace Tests\Feature\Frontend\Users;

use App\Models\Role;
use App\Models\User;
use Database\Seeders\UserSeeder;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

use function PHPUnit\Framework\assertEquals;

class UpdateUserTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    public function test_admin_user_can_update_user(): void
    {
        $faker = Factory::create();
        
        $adminUser = User::where('id', 1)->first();
        $token = $adminUser->createToken('auth-token')->plainTextToken;
        $basicUser = User::where('id', 2)->first();

        $data = [
            'username' => $faker->userName(),
            'password' => '1234567',
            'password_confirmation' => '1234567',
            'role_id' => Role::ADMIN,
            'active' => 0,
        ];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->putJson('/api/users/' . $basicUser->id, $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['message','user']);
            }
        );
        $basicUserUpdated = User::where('id', 2)->first();
        
        $this->assertNotEquals($basicUserUpdated->password, $basicUser->password);

        unset($data['password']);
        unset($data['password_confirmation']);
        $newData = $basicUserUpdated->setHidden(['id','password','remember_token','created_at','updated_at'])->toArray();

        $this->assertEquals($newData, $data);
    }

    public function test_admin_user_can_update_user_whithout_password(): void
    {
        $faker = Factory::create();
        
        $adminUser = User::where('id', 1)->first();
        $token = $adminUser->createToken('auth-token')->plainTextToken;
        $basicUser = User::where('id', 2)->first();

        $data = [
            'username' => $faker->userName(),
            'role_id' => Role::ADMIN,
            'active' => 0,
        ];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->putJson('/api/users/' . $basicUser->id, $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['message','user']);
            }
        );
        $basicUserUpdated = User::where('id', 2)->first();
        
        $this->assertEquals($basicUserUpdated->password, $basicUser->password);

        $newData = $basicUserUpdated->setHidden(['id','password','remember_token','created_at','updated_at'])->toArray();

        $this->assertEquals($newData, $data);
    }

    public function test_admin_user_cannot_update_user_with_duplicate_username(): void
    {
        $faker = Factory::create();
        
        $adminUser = User::where('id', 1)->first();
        $token = $adminUser->createToken('auth-token')->plainTextToken;
        $basicUser = User::where('id', 2)->first();

        $data = [
            'username' => $adminUser->username,
            'role_id' => Role::ADMIN,
            'active' => 0,
        ];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->putJson('/api/users/' . $basicUser->id, $data, $headers);

         //$response->dd();
         $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
         $basicUserUpdated = User::where('id', 2)->first();
         
         $this->assertEquals($basicUserUpdated, $basicUser);
    }

    public function test_admin_user_cannot_update_user_whithout_data(): void
    {
        $faker = Factory::create();
        
        $adminUser = User::where('id', 1)->first();
        $token = $adminUser->createToken('auth-token')->plainTextToken;
        $basicUser = User::where('id', 2)->first();

        $data = [];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->putJson('/api/users/' . $basicUser->id, $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $basicUserUpdated = User::where('id', 2)->first();
        
        $this->assertEquals($basicUserUpdated, $basicUser);
    }

    public function test_basic_user_cannot_update_user(): void
    {
        $faker = Factory::create();
        
        $adminUser = User::where('id', 1)->first();
        $basicUser = User::where('id', 2)->first();
        $token = $basicUser->createToken('auth-token')->plainTextToken;
        
        $data = [];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->putJson('/api/users/' . $adminUser->id, $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $adminUserUpdated = User::where('id', 1)->first();
        
        $this->assertEquals($adminUserUpdated, $adminUser);
    }
}
