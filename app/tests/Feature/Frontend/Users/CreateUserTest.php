<?php

namespace Tests\Feature\Frontend\Users;

use App\Models\Role;
use App\Models\User;
use Database\Seeders\UserSeeder;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    public function test_admin_user_can_create_user(): void
    {
        $faker = Factory::create();
        
        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $data = [
            'username' => $faker->userName(),
            'password' => '123456',
            'password_confirmation' => '123456',
            'role_id' => Role::BASIC,
            'active' => 1,
        ];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->postJson('/api/users', $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_CREATED)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['message','user']);
            }
        );
    }

    public function test_admin_user_cannot_create_user_without_data(): void
    {        
        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->postJson('/api/users', [], $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function test_basic_user_cannot_create_user(): void
    {
        $faker = Factory::create();
        
        $user = User::where('id', 2)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $data = [
            'username' => $faker->userName(),
            'password' => '123456',
            'password_confirmation' => '123456',
            'role_id' => Role::BASIC,
            'active' => 1,
        ];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->postJson('/api/users', $data, $headers);

        //$response->dd();
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
