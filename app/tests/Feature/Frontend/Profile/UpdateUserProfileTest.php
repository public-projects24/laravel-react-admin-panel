<?php

namespace Tests\Feature\Frontend\Profile;

use App\Models\User;
use Database\Seeders\UserSeeder;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateUserProfileTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    public function test_user_can_update_profile(): void
    {
        $faker = Factory::create();

        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;
        
        $data = [
            'username' => $faker->userName(),
            'password' => '1234567',
            'password_confirmation' => '1234567',
        ];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->putJson('/api/profile/' . $user->id, $data, $headers);
        
        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['message','user']);
            }
        );
        
        $userUpdated = User::where('id', 1)->first();
        
        $this->assertNotEquals($userUpdated->password, $user->password);

        unset($data['password']);
        unset($data['password_confirmation']);
        $newData = $userUpdated->setHidden(['id','password','remember_token','role_id','active','created_at','updated_at'])->toArray();

        $this->assertEquals($newData, $data);
    }

    public function test_user_can_update_profile_without_password(): void
    {
        $faker = Factory::create();

        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;
        
        $data = [
            'username' => $faker->userName(),
        ];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->putJson('/api/profile/' . $user->id, $data, $headers);
        
        //$response->dd();
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['message','user']);
            }
        );
        
        $userUpdated = User::where('id', 1)->first();
        
        $this->assertEquals($userUpdated->password, $user->password);

 
        $newData = $userUpdated->setHidden(['id','password','remember_token','role_id','active','created_at','updated_at'])->toArray();

        $this->assertEquals($newData, $data);
    }

    public function test_user_cannot_update_profile_without_data(): void
    {
        $faker = Factory::create();

        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;
        
        $data = [];

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->putJson('/api/profile/' . $user->id, $data, $headers);
        
        //$response->dd();
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        
        $userUpdated = User::where('id', 1)->first();
        
        $this->assertEquals($userUpdated, $user);
    }
}
