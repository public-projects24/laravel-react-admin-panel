<?php

namespace Tests\Feature\Frontend\Profile;

use App\Models\User;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class ShowUserProfileTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;


    public function test_user_can_view_profile(): void
    {
        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->getJson('/api/profile', $headers);
        
        //$response->dd();
        
        $response->assertStatus(Response::HTTP_OK)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['data']);
            }
        );
    }
}
