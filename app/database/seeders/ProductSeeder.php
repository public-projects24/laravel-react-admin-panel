<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Product::factory()->create(['name' => 'comp desk 1', 'subcategory_id' => 1]);
        Product::factory()->create(['name' => 'comp desk 2', 'subcategory_id' => 1]);
        Product::factory()->create(['name' => 'comp lapt 1', 'subcategory_id' => 2]);
        Product::factory()->create(['name' => 'comp lapt 2', 'subcategory_id' => 2]);

        Product::factory()->create(['name' => 'game horr 1', 'subcategory_id' => 3]);
        Product::factory()->create(['name' => 'game horr 2', 'subcategory_id' => 3]);
        Product::factory()->create(['name' => 'game adven 1', 'subcategory_id' => 4]);
        Product::factory()->create(['name' => 'game adven 2', 'subcategory_id' => 4]);

        Product::factory(20)->create();

    }
}
