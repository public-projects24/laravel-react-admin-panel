<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\UserInformation;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()
        ->create([
            'username' => 'admin',
            'password' => '123456',
            'role_id' => Role::ADMIN,
            'active' => 1
       ]);

       User::factory()
        ->create([
            'username' => 'basic',
            'password' => '123456',
            'role_id' => Role::BASIC,
            'active' => 1
       ]);
       
        User::factory(20)->create([
            'role_id' => Role::BASIC,
            'active' => 0
        ]);

        User::factory(30)->create();
    }
}
