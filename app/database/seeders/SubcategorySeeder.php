<?php

namespace Database\Seeders;

use App\Models\Subcategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Subcategory::factory()->create(['name' => 'desktop','category_id' => 1, 'active' => 1]);
        Subcategory::factory()->create(['name' => 'laptop','category_id' => 1, 'active' => 1]);

        Subcategory::factory()->create(['name' => 'horror','category_id' => 2, 'active' => 1]);
        Subcategory::factory()->create(['name' => 'adventure','category_id' => 2, 'active' => 1]);

        //Subcategory::factory(10)->create(['category_id' => 1]);
    }
}
