## Laravel React Admin Panel

[Laravel React Admin Panel](http://3.95.116.225:83/) is a dashboard for manage users, categories, subcategories, and products.

## Credentials

```bash
email: admin
password: 123456
```

```bash
email: basic
password: 123456
```

## Getting Started

First, you need to have ["Docker"](https://www.docker.com/) installed in your machine
then:

1. Download this repository.
2. Initialize the container and install the composer packages and npm packages:

```bash
make up
make serv
composer install
npm install
```

3. Set the correct permissions and group for these folders:

```bash
chgrp -R www-data storage
chmod -R 770 storage

chgrp -R www-data bootstrap/cache
chmod -R 770 bootstrap/cache
```

3. Go to "app" folder and duplicate the ".env.example" with the name ".env"

4. Generate the application key

```bash
php artisan key:generate
```

5. Run the database migration and seeders

```bash
php artisan migrate --seed
```

6. Run tests

```bash
php artisan test
```

7. Run dev mode

```bash
npm run dev
```

8. Open [http://localhost:9000](http://localhost:9000) with your browser.

## Live Demo

Open [http://3.95.116.225:83/](http://3.95.116.225:83/) with your browser to see a live preview.
